// F16 CS 401 Speeding Ticket Project
// Project1.java Starter File

import java.io.*;
import java.util.*;

public class Project1
{
	public static void main (String args[])
	{
		// Create a scanner to read from keyboard
		Scanner kbd = new Scanner (System.in);

		String firstName="N/A",lastName="N/A";
		int age=0, speedLimit=0, actualSpeed=0, mphOverLimit=0;
		int baseFine=0, underAgeFine=0, zoneFine=0, totalFine=0;
		// DO NOT ADD TO OR MODIFY ABOVE THIS LINE
		
		boolean inZone;

		System.out.print("Enter you first and last name: ");
		
		if (kbd.hasNext()) {
			
			firstName = kbd.next();
			lastName = kbd.next();
		}
		
		System.out.print("Enter your age: ");
		age = kbd.nextInt();
		
		System.out.print("Enter the posted speed limit: ");
		speedLimit = kbd.nextInt();
		
		System.out.print("Enter the speed you were clocked at: ");
		actualSpeed = kbd.nextInt();
		
		System.out.print("Was the violation recorded in a construction zone? (true/false): ");
		inZone = kbd.nextBoolean();
		
		mphOverLimit = actualSpeed - speedLimit;
		
		if (mphOverLimit > 5) {
			if (mphOverLimit <= 20)
				baseFine = (mphOverLimit / 5) * 30;
			
			else if (mphOverLimit > 20)
				baseFine = (mphOverLimit / 5) * 50;
			
			if (inZone == true)
				zoneFine = baseFine;
			
			if (age < 21 && mphOverLimit > 20)
				underAgeFine = 300;
			
			totalFine = baseFine + underAgeFine + zoneFine;
		}
			

		// DO NOT ADD TO OR MODIFY BELOW THIS LINE
		System.out.println();
		System.out.format( "name: %s, %s\n",lastName,firstName );
		System.out.format( "age: %d yrs.\n",age );
		System.out.format( "actual speed: %d mph.\n",actualSpeed );
		System.out.format( "speed limit: %d mph.\n",speedLimit );
		System.out.format( "mph over limit: %d mph.\n",mphOverLimit );
		System.out.format( "base fine: $%d\n",baseFine );
		System.out.format( "zone fine: $%d\n",zoneFine );
		System.out.format( "under age fine: $%d\n",underAgeFine );
		System.out.format( "total fine: $%d\n",totalFine );
	} // END MAIN
} // END PROJECT1 CLASS
